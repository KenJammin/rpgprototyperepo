﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour {

	public string projectileName;
	public bool playerProjectile;
	public float rotationSpeed;
	public float movementSpeed;
	public float fadeSpeed;
	private GameObject battleManager;
	private bool active;
	private Vector3 tempPos;
	private Color spriteColor;

	void Start () {
		active = true;
		tempPos = transform.position;
		battleManager = GameObject.FindGameObjectWithTag ("BattleManager");
		spriteColor = GetComponent<SpriteRenderer>().color;
	}

	void FixedUpdate () {
		if (active) {
			if (playerProjectile) {
				tempPos.y += movementSpeed * Time.deltaTime;
				transform.position = tempPos;
			} else {
				tempPos.y -= movementSpeed * Time.deltaTime;
				transform.position = tempPos;
			}
			if (rotationSpeed > 0) {
				transform.Rotate (0, 0, -Time.deltaTime * rotationSpeed);
			}
		} else {
			spriteColor.a -= fadeSpeed * Time.deltaTime;
			GetComponent<SpriteRenderer> ().color = spriteColor;
		}
	}

	void OnTriggerEnter2D(Collider2D other) {
		if (other.gameObject.tag == "Enemy" && active && playerProjectile) {
			battleManager.GetComponent<NewBattleManager>().ProjectileHit(projectileName, playerProjectile, this.transform.position);
			active = false;
		}
		if (other.gameObject.tag == "Player" && active && !playerProjectile) {
			battleManager.GetComponent<NewBattleManager>().ProjectileHit(projectileName, playerProjectile, this.transform.position);
			active = false;
		}
	}
}
