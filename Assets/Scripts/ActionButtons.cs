﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActionButtons : MonoBehaviour {
	public int buttonID;
	public string buttonType;
	public GameObject battleManager;
	public Sprite greenButtonSprite;
	public Sprite greenButtonPushedSprite;
	public Sprite redButtonSprite;
	public Sprite redButtonPushedSprite;
	public Sprite blueButtonSprite;
	public Sprite blueButtonPushedSprite;
	void Start () {
	}
	public void UpdateButtonImage(string bType) {
		if (bType == "support") {
			buttonType = "support";
			GetComponent<SpriteRenderer> ().sprite = greenButtonSprite;
		}
		if (bType == "attack") {
			buttonType = "attack";
			GetComponent<SpriteRenderer> ().sprite = redButtonSprite;
		}
		if (bType == "ability") {
			buttonType = "ability";
			GetComponent<SpriteRenderer> ().sprite = blueButtonSprite;
		}
	}
	void OnMouseDown () {
		if (buttonType == "support") {
			GetComponent<SpriteRenderer> ().sprite = greenButtonPushedSprite;
		}
		if (buttonType == "attack") {
			GetComponent<SpriteRenderer> ().sprite = redButtonPushedSprite;
		}
		if (buttonType == "ability") {
			GetComponent<SpriteRenderer> ().sprite = blueButtonPushedSprite;
		}
			battleManager.GetComponent<NewBattleManager> ().ButtonPressed(buttonID);
	}
	void OnMouseUp(){
		if (buttonType == "support") {
			GetComponent<SpriteRenderer> ().sprite = greenButtonSprite;
		}
		if (buttonType == "attack") {
			GetComponent<SpriteRenderer> ().sprite = redButtonSprite;
		}
		if (buttonType == "ability") {
			GetComponent<SpriteRenderer> ().sprite = blueButtonSprite;
		}
	}
}
