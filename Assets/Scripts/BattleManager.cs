﻿/*
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BattleManager : MonoBehaviour {
	//Public Declarations
	public float sideWindowWaitTime;
	public float buttonMoveWaitTime;
	public float hitDelay;
	//Public GameObjects
	public GameObject playerInfo;
	public GameObject enemyInfo;
	public GameObject damageText;
	public GameObject playerSideWindow;
	public GameObject enemySideWindow;
	public GameObject bottomWindowHandler;
	public GameObject topWindowHandler;
	public GameObject button1;
	public GameObject button2;
	public GameObject button3;
	// Player Declarations
	private int playerLevel;
	private int playerHP;
	private int playerCurrentHP;
	private int playerMaxHP;
	private int playerCurrentExp;
	private int playerNextLevelExp;
	private int playerSupportType;
	private int playerAttackType;
	private int playerDefendType;
	private string supportButton1;
	private string supportButton2;
	private string supportButton3;
	private string attackButton1;
	private string attackButton2;
	private string attackButton3;
	private string defendButton1;
	private string defendButton2;
	private string defendButton3;
	// Enemy Declarations
	private int enemyLevel;
	private int enemyCurrentHP;
	private int enemyMaxHP;
	private string enemySupportSkill1;
	private string enemySupportSkill2;
	private string enemySupportSkill3;
	private string enemyAttackSkill1;
	private string enemyAttackSkill2;
	private string enemyAttackSkill3;
	private string enemyDefendSkill1;
	private string enemyDefendSkill2;
	private string enemyDefendSkill3;
	private string enemyName;
	// System Declarations
	public bool startCallSideWindow;
	private bool moveOldPhase;
	public string phase;
	private float wait;
	private float buttonMoveWait;
	private int playerTempHPChange;
	private int enemyTempHPChange;
	private float playerHPwait;
	private float enemyHPwait;
	private float hitDelayTimer;
	private int hitCounter;
	private float hpTickDelay;
	private float damage;
	private string enemyActionSelected;
	private string playerActionSelected;
	//active actions/buffs/debuffs
	public bool playerAttackBoost;
	public bool playerGuard;
	public bool playerCounter;
	void Start () {
		playerSupportType = 1;
		playerAttackType = 1;
		playerDefendType = 1;
		playerLevel = 1;
		playerMaxHP = 10;
		playerCurrentHP = 10;
		playerCurrentExp = 0;
		playerNextLevelExp = 10;
		enemyName = "ENEMY";
		enemyCurrentHP = 10;
		enemyMaxHP = 10;
		BattleStart ();
	}
	void Update () {
		playerInfo.GetComponent<Text> ().text = "LEVEL\n" + playerLevel.ToString() + "\n\n" + "HEALTH\n" + playerCurrentHP.ToString () + "/" + playerMaxHP.ToString () + "\n\n" + "EXP\n" + playerCurrentExp.ToString() + "/" + playerNextLevelExp.ToString();
		enemyInfo.GetComponent<Text> ().text = "LEVEL\n" + enemyLevel.ToString() + "\n\n" + "HP\n" + enemyCurrentHP.ToString () + "/" + enemyMaxHP.ToString () + "\n\n" + enemyName + "\n";
	}

	void FixedUpdate() {
		// DelayAttacks
		if (hitCounter > 0) {
			if (hitDelayTimer > 0) {
				hitDelayTimer -= 1 * Time.deltaTime;
			} else {
				hitDelayTimer = hitDelay;
				ChangeHP(true,1000,"enemy");
				hitCounter -= 1;
			}
		}
		if (playerCounter) {
			if (hitDelayTimer > 0) {
				hitDelayTimer -= 1 * Time.deltaTime;
			} else {
				PlayerAction ("COUNTER ATTACK");
				playerCounter = false;
			}
		} 
		// HP Tick Timer
		if (playerHPwait >= 0) playerHPwait -= 1 * Time.deltaTime;
		if (enemyHPwait >= 0) enemyHPwait -= 1 * Time.deltaTime;
		// Update HP
		if(playerHPwait <=0) {
			if (playerTempHPChange < playerCurrentHP) {
				playerCurrentHP -= 1;
			} else if (playerTempHPChange > playerCurrentHP){
				playerCurrentHP += 1;
			}
			playerHPwait = hpTickDelay;
		}
		if (enemyHPwait <= 0) {
			if (enemyTempHPChange < enemyCurrentHP) {
				enemyCurrentHP -= 1;
			} else if (enemyTempHPChange > enemyCurrentHP) {
				enemyCurrentHP += 1;
			}
			enemyHPwait = hpTickDelay;
			if (enemyCurrentHP <= 0) BattleStart ();
			// need to resend the switch phase button command because the first time it's sent it wont happen if there's a counter attack, only done if the target still has remaining HP.
			if (enemyCurrentHP > 0 && playerCounter) SwitchPhaseButtons ();
		}
		// Move In the side window
		if (startCallSideWindow) {
			if (wait > 0) {
				wait -= 1 * Time.deltaTime;
			} else {
// This is where actions are initiated and resolved and the side window's are moved away.
				PlayerAction (playerActionSelected);
				EnemyAction (enemyActionSelected);
				MoveSideWindow (false);
			}
		}
		// ChangePhases
		if (moveOldPhase) {
			if (buttonMoveWait > 0) {
				buttonMoveWait -= 1 * Time.deltaTime;
			} else {
				moveOldPhase = false;
				if (enemyTempHPChange > 0 && !playerCounter) {
					bottomWindowHandler.GetComponent<ButtonMovement> ().MoveInNextPhase ();
					topWindowHandler.GetComponent<ButtonMovement> ().MoveInNextPhase ();
					PhaseChange ();
				}
			}
		}
	}
	private void BattleStart(){
		playerTempHPChange = playerCurrentHP;
		PhaseTransition ("support");
		phase = "support";
		bottomWindowHandler.GetComponent<ButtonMovement> ().MoveInNextPhase();
		topWindowHandler.GetComponent<ButtonMovement> ().MoveInNextPhase ();
		SpawnEnemy ("WILD DOG");
		enemyTempHPChange = enemyCurrentHP;
	}
	private void BattleEnd() {
		bottomWindowHandler.GetComponent<ButtonMovement> ().MoveOutCurrentPhase();
		topWindowHandler.GetComponent<ButtonMovement> ().MoveOutCurrentPhase ();
		BattleStart ();
	}
	public void SpawnEnemy(string enemyFlag) {
		enemyName = enemyFlag;
		if (enemyName == "WILD DOG") {
			enemyCurrentHP = 10;
			enemyMaxHP = 10;
			enemyLevel = 1;
			enemySupportSkill1 = "BARK";
			enemySupportSkill2 = "HOWL";
			enemyAttackSkill1 = "BITE";
			enemyAttackSkill2 = "RUSH DOWN";
			enemyDefendSkill1 = "SIDE STEP";
			enemyDefendSkill2 = "COUNTER";
		}
	}
	public void PlayerAction(string abilityUsed) {
		bool attackUsed = false;
		if (abilityUsed == "ATTACK BOOST") {
			playerAttackBoost = true;
		}
		if (abilityUsed == "FIRST AID") {
			ChangeHP (false, playerLevel * 2, "player");
		}
		if (abilityUsed == "SLASH") {
			attackUsed = true;
			damage = playerLevel + Random.Range(5,10) + (playerLevel - enemyLevel);
			if (playerAttackBoost) {
				damage = damage * 1.5f;
			}
			playerAttackBoost = false;
		}
		if (abilityUsed == "GUARD") {
			playerGuard = true;
		}
		if (abilityUsed == "COUNTER") {
			playerCounter = true;
			hitDelayTimer = hitDelay;
		}
		if (abilityUsed == "COUNTER ATTACK") {
			attackUsed = true;
			damage = playerLevel + Random.Range (5, 10) + (playerLevel - enemyLevel);
		}
		if (attackUsed) {
			ChangeHP(true,Mathf.RoundToInt(damage),"enemy");
		}
	}
	public void EnemyAction(string enemySkill) {
		bool attackUsed = false;
		if (enemySkill == "BITE") {
			attackUsed = true;
			damage = enemyLevel + Random.Range (1, 5) + (enemyLevel - playerLevel);
			if (playerGuard) damage = damage * 0.5f;
		}
		if (attackUsed) {
			ChangeHP(true,Mathf.RoundToInt(damage),"player");
		}
		if (playerGuard) playerGuard = false;
	}
	public void ButtonPressed(int buttonID) {
		EnemyTurn ();
		SwitchPhaseButtons ();
		if (buttonID == 1) {
			if (phase == "support") playerActionSelected = supportButton1;
			if (phase == "attack") playerActionSelected = attackButton1;
			if (phase == "defend") playerActionSelected = defendButton1;
		}
		if (buttonID == 2) {
			if (phase == "support") playerActionSelected = supportButton2;
			if (phase == "attack") playerActionSelected = attackButton2;
			if (phase == "defend") playerActionSelected = defendButton2;
		}
		if (buttonID == 3) {
			if (phase == "support") playerActionSelected = supportButton3;
			if (phase == "attack") playerActionSelected = attackButton3;
			if (phase == "defend") playerActionSelected = defendButton3;
		}
		CallSideWindow (playerActionSelected);
	}
	public void EnemyTurn() {
		if (phase == "support") {
			if (Random.Range (1, 2) == 1) {
				enemyActionSelected = enemySupportSkill1;
			} else {
				enemyActionSelected = enemySupportSkill2;
			}
		}
		if (phase == "attack") {
			if (Random.Range (1, 2) == 1) {
				enemyActionSelected = enemyDefendSkill1;
			} else {
				enemyActionSelected = enemyDefendSkill2;
			}
		}
		if (phase == "defend") {
			if (Random.Range (1, 1) == 1) {
				enemyActionSelected = enemyAttackSkill1;
			} else {
				enemyActionSelected = enemyAttackSkill2;
			}
		}
	}
	private void SwitchPhaseButtons(){
		moveOldPhase = true;
		buttonMoveWait = buttonMoveWaitTime;
		bottomWindowHandler.GetComponent<ButtonMovement> ().MoveOutCurrentPhase();
		topWindowHandler.GetComponent<ButtonMovement> ().MoveOutCurrentPhase ();
	}
	public void PhaseChange() {
		if (phase == "support") {
			phase = "attack";
			PhaseTransition ("attack");
		}
		else if (phase == "attack") {
			phase = "defend";
			PhaseTransition ("defend");
		}
		else if (phase == "defend"){
			phase = "support";
			PhaseTransition ("support");
		}
	}
	public void PhaseTransition(string phaseFlag){
		UpdateAbilities ();
		if (phaseFlag == "support") {
			button1.GetComponent<ActionButtons> ().UpdateButtonImage ("support");
			button2.GetComponent<ActionButtons> ().UpdateButtonImage ("support");
			button3.GetComponent<ActionButtons> ().UpdateButtonImage ("support");
			button1.transform.GetChild (0).GetChild (0).GetComponent<Text> ().text = supportButton1;
			button2.transform.GetChild (0).GetChild (0).GetComponent<Text> ().text = supportButton2;
			button3.transform.GetChild (0).GetChild (0).GetComponent<Text> ().text = supportButton3;
		}
		if (phaseFlag == "attack") {
			button1.GetComponent<ActionButtons> ().UpdateButtonImage ("attack");
			button2.GetComponent<ActionButtons> ().UpdateButtonImage ("attack");
			button3.GetComponent<ActionButtons> ().UpdateButtonImage ("attack");
			button1.transform.GetChild (0).GetChild (0).GetComponent<Text> ().text = attackButton1;
			button2.transform.GetChild (0).GetChild (0).GetComponent<Text> ().text = attackButton2;
			button3.transform.GetChild (0).GetChild (0).GetComponent<Text> ().text = attackButton3;
		}
		if (phaseFlag == "defend") {
			button1.GetComponent<ActionButtons> ().UpdateButtonImage ("defend");
			button2.GetComponent<ActionButtons> ().UpdateButtonImage ("defend");
			button3.GetComponent<ActionButtons> ().UpdateButtonImage ("defend");
			button1.transform.GetChild (0).GetChild (0).GetComponent<Text> ().text = defendButton1;
			button2.transform.GetChild (0).GetChild (0).GetComponent<Text> ().text = defendButton2;
			button3.transform.GetChild (0).GetChild (0).GetComponent<Text> ().text = defendButton3;
		}
	}
	private void UpdateAbilities(){
		if (playerSupportType == 1) {
			supportButton1 = "ATTACK BOOST";
			supportButton2 = "DOUBLE ATTACK";
			supportButton3 = "FIRST AID";
		}
		if (playerAttackType == 1) {
			attackButton1 = "SLASH";
			attackButton2 = "POISON";
			attackButton3 = "OVER POWER";
		}
		if (playerDefendType == 1) {
			defendButton1 = "GUARD";
			defendButton2 = "COUNTER";
			defendButton3 = "EVADE";
		}
	}
	public void ChangeHP(bool lose, int hpChange, string target){
		if (target == "player") {
			if(lose) playerTempHPChange = playerCurrentHP  - hpChange;
			if (!lose) playerTempHPChange = playerCurrentHP + hpChange;
		}
		if (target == "enemy") {
			if(lose) enemyTempHPChange = enemyTempHPChange  - hpChange;
			if (!lose) enemyTempHPChange = enemyTempHPChange + hpChange;
		}
		if (hpChange < 10) {
			hpTickDelay = 0.1f;
		} else {
			hpTickDelay = 1 / hpChange;
		}
		if (enemyTempHPChange <= 0) enemyTempHPChange = 0;
		if (playerTempHPChange <= 0) playerTempHPChange = 0;
		ShowDamage (hpChange,target);
	}
	public void ShowDamage(int damage, string target) {
		if (target == "player") {
			GameObject spawnedTextInstantiate = Instantiate (damageText, damageText.transform.position, Quaternion.identity);
			GameObject spawnedText = spawnedTextInstantiate.transform.GetChild (0).GetChild (0).gameObject;
			spawnedText.GetComponent<Text> ().text = damage.ToString();
		} 
		if(target == "enemy"){
			GameObject spawnedTextInstantiate = Instantiate (damageText, damageText.transform.position, Quaternion.identity);
			GameObject spawnedText = spawnedTextInstantiate.transform.GetChild (0).GetChild (0).gameObject;
			spawnedText.GetComponent<Text> ().text = damage.ToString();
		}
	}
	public void CallSideWindow(string playerAction){
		startCallSideWindow = true;
		wait = sideWindowWaitTime;
		MoveSideWindow (true);
		playerSideWindow.transform.GetChild(0).GetChild(0).GetComponent<Text>().text = playerAction;
		enemySideWindow.transform.GetChild(0).GetChild(0).GetComponent<Text>().text = enemyActionSelected;
	}
	public void MoveSideWindow(bool windowCalled){
		if (windowCalled) {
			playerSideWindow.GetComponent<SideActionWindow> ().WindowCalled (true);
			enemySideWindow.GetComponent<SideActionWindow> ().WindowCalled (true);
		} else {
			playerSideWindow.GetComponent<SideActionWindow> ().WindowCalled (false);
			enemySideWindow.GetComponent<SideActionWindow> ().WindowCalled (false);
			startCallSideWindow = false;
		}
	}
}


*/