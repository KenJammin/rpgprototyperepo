﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DamageText : MonoBehaviour {
	public bool enemyDamageText;
	public GameObject damageText;
	public Color textColor;
	public float fadeSpeed;
	public float moveSpeed;
	public float delay;
	private Vector3 tempPos;
	void Start () {
		tempPos = transform.position;
	}

	// Update is called once per frame
	void FixedUpdate () {
		if (delay > 0) {
			delay -= 1 * Time.deltaTime;
		} else {
			textColor.a -= fadeSpeed * Time.deltaTime;
			damageText.GetComponent<Text> ().color = textColor;
			if (enemyDamageText) {
				tempPos.y += moveSpeed * Time.deltaTime;
			} else {
				tempPos.y -= moveSpeed * Time.deltaTime;
			}
			transform.position = tempPos;
		}
		if (textColor.a <= 0)
			Destroy (this.gameObject);
	}
}