﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class NewBattleManager : MonoBehaviour {
	public GameObject playerInfo;
	public GameObject enemyInfo;
	public GameObject bottomWindowHandler;
	public GameObject playerWindowHandler;
	public GameObject enemyWindowHandler;
	public GameObject windowSideBlueLeft;
	public GameObject windowSideBlueRight;
	public GameObject windowSideRedLeft;
	public GameObject windowSideRedRight;
	public GameObject playerWindow;
	public GameObject enemyWindow;
	public GameObject[] playerAttackPrefabs;
	public GameObject[] enemyAttackPrefabs;
	public GameObject button1;
	public GameObject button2;
	public GameObject button3;
	public GameObject button4;
	public GameObject enemyDamageText;
	public GameObject playerDamageText;
	public GameObject BottomWindowDisabled;
	public GameObject retryWindow;
	public GameObject playerHealthBar;
	public GameObject enemyHealthBar;
	public GameObject playerStaminaBar;
	public GameObject enemyStaminaBar;
	public GameObject[] playerBuffContainer;
	public GameObject[] playerStatusContainer;
	public GameObject[] enemyBuffContainer;
	public GameObject[] enemyStatusContainer;
	public Sprite[] buffSpriteContainer;
	public Sprite[] statusSpriteContainer;
	public GameObject[] textGameObjectContainer;
	public Text[] textContainer;

	private int playerLevel;
	private int playerMaxHP;
	private int playerCurrentHP;
	private int playerCurrentExp;
	private int playerNextLevelExp;
	private int playerPower;
	private int playerDefense;
	private int playerLuck;
	private int oreCount;
	private int manaCount;
	private int spiritCount;
	private float productionTimer;
	private int enemyLevel;
	private int enemyMaxHP;
	private int enemyCurrentHP;
	private string enemyName;
	private int enemyPower;
	private int enemyDefense;
	private int enemyLuck;
	private float enemyActionTimer;
	private float enemyActionSpeed;
	private string enemyNextAction;
	private string battleState;
	private float battleStateWait;
	private float[] enemyStatus = new float[6];
	private float[] enemyBuffs = new float[6]; 
	private float[] playerStatus = new float[6]; // 0 = Attack Down, 1 = Defense Down, 2 = Fire, 3 = slow, 4 = stun, 5 = toxic;
	private float[] playerBuffs = new float[6]; // 0 = Attack Up, 1 = Defense Up, 2 = Haste, 3 = LifeSteal, 4 = Regen, 5 = Counter;
	private string playerStatusString;
	private string playerBuffsString;
	private float statusTimer;
	private string activeButton1;
	private string activeButton2;
	private string activeButton3;
	private string activeButton4;
	private SpriteRenderer[] playerBuffSpriteRenderer = new SpriteRenderer[3];
	private SpriteRenderer[] enemyBuffSpriteRenderer = new SpriteRenderer[3];
	private SpriteRenderer[] playerStatusSpriteRenderer = new SpriteRenderer[3];
	private SpriteRenderer[] enemyStatusSpriteRenderer = new SpriteRenderer[3];

	void Start () {
		for (int i = 0; i < playerStatusContainer.Length; i++) { 		// loop through and assign sprite renderer so that get Component isn't being called a bunch each frame.
			playerStatusSpriteRenderer[i] = playerStatusContainer[i].GetComponent<SpriteRenderer> ();
		}
		for (int i = 0; i < enemyStatusContainer.Length; i++) {
			enemyStatusSpriteRenderer[i] = enemyStatusContainer[i].GetComponent<SpriteRenderer> ();
		}
		for (int i = 0; i < playerBuffContainer.Length; i++) {
			playerBuffSpriteRenderer[i] = playerBuffContainer[i].GetComponent<SpriteRenderer> ();
		}
		for (int i = 0; i < enemyBuffContainer.Length; i++) {
			enemyBuffSpriteRenderer[i] = enemyBuffContainer[i].GetComponent<SpriteRenderer> ();
		}
		textContainer = new Text[textGameObjectContainer.Length]; // store the size of the container in a new array so that if I add more text objects in the future I wont need to increment the array length.
		for (int i = 0; i < textGameObjectContainer.Length; i++) {
			textContainer [i] = textGameObjectContainer [i].GetComponent<Text> ();
		}
		LoadPlayer ();
		EnemySpawn ("slime"); // dummy spawn. Prevents enemy variables from being null and causing NaN errors.
		UpdateStatus();
	}
	
	void Update () {
		if (statusTimer >= 0) {
			statusTimer -= 1 * Time.deltaTime;
		} else {
			statusTimer = 1;
			ApplyStatus ();
		}
		if (enemyCurrentHP <= 0) {
			enemyCurrentHP = 0;
			ClearStatus (); // clear status so that new enemy doesn't load in with status effect still on.
			if(battleState == "activeBattle")EnemyDeath (); // check to make sure EnemyDeath() doesn't run when new enemy is loaded.
		}
		if (playerCurrentHP <= 0) {
			playerCurrentHP = 0;
			PlayerDeath();
		}
		if (enemyCurrentHP >= enemyMaxHP) enemyCurrentHP = enemyMaxHP;
		if (playerCurrentHP >= playerMaxHP) playerCurrentHP = playerMaxHP;

		float playerHealthPercentage = (float)playerCurrentHP / (float)playerMaxHP;
		playerHealthBar.transform.localScale = new Vector3 (playerHealthPercentage, 1, 1);
		float enemyHealthPercentage = (float)enemyCurrentHP / (float)enemyMaxHP;
		enemyHealthBar.transform.localScale = new Vector3 (enemyHealthPercentage, 1, 1);
		playerInfo.GetComponent<Text> ().text = playerCurrentHP.ToString () + "/" + playerMaxHP.ToString ();
		enemyInfo.GetComponent<Text> ().text = enemyName + "\n" + "\n\n\n" + "\n\n" + "\n\n" + enemyCurrentHP.ToString () + "/" + enemyMaxHP.ToString ();
		windowSideBlueLeft.transform.GetChild(0).GetChild(0).GetComponent<Text>().text = "\n\n\n\n" + "ORE:" + oreCount.ToString() + "\n" + "MANA:" + manaCount.ToString();
		windowSideBlueRight.transform.GetChild (0).GetChild (0).GetComponent<Text> ().text = "LEVEL\n" + playerLevel.ToString() + "\n\n" + "POW: " + playerPower.ToString () + "\n" + "DEF: " + playerDefense.ToString () + "\n" + "LUK: " + playerLuck.ToString () + "\n" + "EXP\n" + playerCurrentExp.ToString() + "/" + playerNextLevelExp.ToString();
		windowSideRedLeft.transform.GetChild (0).GetChild (0).GetComponent<Text> ().text = "NEXT:\n" + enemyNextAction;
		windowSideRedRight.transform.GetChild (0).GetChild (0).GetComponent<Text> ().text = "LEVEL\n" + enemyLevel.ToString()+ "\n\n" + "POW: " + enemyPower.ToString () + "\n" + "DEF: " + enemyDefense.ToString () + "\n" + "LUK: " + enemyLuck.ToString ();


		if (battleState == "activeBattle") {
			productionTimer += Time.deltaTime * 25;
			enemyActionTimer += Time.deltaTime * enemyActionSpeed;
			if (productionTimer >= 100) {
				productionTimer = 0;
				oreCount += 3;
				manaCount += 1;
				if (oreCount > 20) oreCount = 20;
				if (manaCount > 10) manaCount = 10;
			}
			float playerStaminaPercentage = (float)productionTimer / 100;
			playerStaminaBar.transform.localScale = new Vector3 (playerStaminaPercentage, 1, 1);
			if (enemyActionTimer >= 100) {
				enemyActionTimer = 0;
				EnemyAttack (enemyNextAction);
				EnemyAttackSelect ();
			}
			float enemyStaminaPercentage = (float)enemyActionTimer / 100;
			enemyStaminaBar.transform.localScale = new Vector3 (enemyStaminaPercentage, 1, 1);
			for (int i = 0; i < enemyStatus.Length; i++) {
				if(enemyStatus [i] > 0) enemyStatus[i] -= 1 * Time.deltaTime;
			}
			for (int i = 0; i < enemyBuffs.Length; i++) {
				if(enemyBuffs [i] > 0) enemyBuffs[i] -= 1 * Time.deltaTime;
			}
			for (int i = 0; i < playerStatus.Length; i++) {
				if(playerStatus [i] > 0) playerStatus[i] -= 1 * Time.deltaTime;
			}
			for (int i = 0; i < playerBuffs.Length; i++) {
				if(playerBuffs [i] > 0) playerBuffs[i] -= 1 * Time.deltaTime;
			}
			UpdateStatus ();
		} 		// end of active battle if statement;
		if (battleStateWait >= 0) battleStateWait -= 1f * Time.deltaTime;
		if (battleState == "loadPlayer" && battleStateWait <= 0) LoadEnemy();
		if (battleState == "loadEnemy" && battleStateWait <= 0) BattleStart();
		if (battleState == "enemyDeath" && battleStateWait <= 0) {
			battleState = "moveOutEnemy";
			EnemyDeath ();
		}
		if (battleState == "moveOutEnemy" && battleStateWait <=0) LoadEnemy();
	}	// end of Update function;

	void BattleStart() {
		battleState = "activeBattle";
		BottomWindowDisabled.SetActive (false);
	}
	void LoadPlayer() {
		battleState = "loadPlayer";
		battleStateWait = 1;
		UpdateButtons ("weapons");
		UpdatePlayer(1);
		playerWindowHandler.GetComponent<WindowMovement> ().MoveInWindow ();
		bottomWindowHandler.GetComponent<WindowMovement> ().MoveInWindow();
		windowSideBlueLeft.GetComponent<WindowMovement> ().MoveInWindow();
		windowSideBlueRight.GetComponent<WindowMovement> ().MoveInWindow();
	}

	void LoadEnemy() {
		battleState = "loadEnemy";
		battleStateWait = 1;
		EnemySpawn ("slime");
		enemyWindowHandler.GetComponent<WindowMovement> ().MoveInWindow ();
		windowSideRedLeft.GetComponent<WindowMovement> ().MoveInWindow();
		windowSideRedRight.GetComponent<WindowMovement> ().MoveInWindow();
	}
	void EnemyDeath() {
		if (battleState == "activeBattle") {
			battleState = "enemyDeath";
			battleStateWait = 1f;
			BottomWindowDisabled.SetActive (true);
			GameObject[] enemyProjectileCollections = GameObject.FindGameObjectsWithTag("EnemyProjectile");
			foreach (GameObject destruct in enemyProjectileCollections) {
				Destroy (destruct);
			}
			ClearStatus ();
			productionTimer = 0;
			enemyActionTimer = 0;
		}
		if (battleState == "moveOutEnemy") {
			battleStateWait = 1f;
			enemyWindowHandler.GetComponent<WindowMovement> ().MoveOutWindow ();
			windowSideRedLeft.GetComponent<WindowMovement> ().MoveOutWindow();
			windowSideRedRight.GetComponent<WindowMovement> ().MoveOutWindow();
		}
	}
	void PlayerDeath() {
		battleState = "playerDeath";
		GameObject [] enemyProjectileCollections = GameObject.FindGameObjectsWithTag("EnemyProjectile");
		foreach (GameObject destruct in enemyProjectileCollections) {
			Destroy (destruct);
		}
		GameObject[] playerProjectileCollections = GameObject.FindGameObjectsWithTag("PlayerProjectile");
		foreach (GameObject destruct in playerProjectileCollections) {
			Destroy (destruct);
		}
		ClearStatus ();
		retryWindow.SetActive (true);
	}


	void EnemySpawn(string enemyLoaded) {
		if (enemyLoaded == "slime") {
			enemyName = "SLIME";
			enemyLevel = 1;
			enemyMaxHP = enemyLevel * 25;
			enemyCurrentHP = enemyMaxHP;
			enemyPower = StatCalc (2, enemyLevel);
			enemyLuck = StatCalc (1, enemyLevel);
			enemyDefense = StatCalc(2, enemyLevel);
		}
		EnemyAttackSelect ();
	}
	void EnemyAttackSelect() {
		int randomNum;
		if (enemyName == "SLIME") {
			randomNum = Random.Range (1, 3);
			if (randomNum == 1) {
				enemyNextAction = "RECOVER";
				enemyActionSpeed = 20;
			} else {
				enemyNextAction = "SLUDGE";
				enemyActionSpeed = 35;
			}
		}
	}
	public void EnemyAttack(string enemyAction) {
		float randomProjectileStartPosition = Random.Range (-1.25f, 1.25f);
		Vector3 tempPos = enemyWindow.transform.position;
		tempPos.x += randomProjectileStartPosition;
		if (enemyAction == "SLUDGE") {
			Instantiate (enemyAttackPrefabs [0], tempPos, enemyWindow.transform.rotation);
		}
		if (enemyAction == "RECOVER") {
			enemyCurrentHP += 1 * enemyLevel;
		}
	}


	void UpdatePlayer(int level) {
		playerLevel = level;
		playerMaxHP = 10 + 10 * level;
		playerCurrentHP = playerMaxHP;
		playerCurrentExp = 0;
		playerNextLevelExp = 10 + playerLevel * 2;
		playerPower = StatCalc (4, playerLevel);
		playerLuck = StatCalc (2, playerLevel);
		playerDefense = StatCalc (3, playerLevel);
	}

	int StatCalc(int rating, int playerLevel) {
		int statValue = 0;
		statValue = (playerLevel * rating / 2) + 1;
		return statValue;
	}
	public void ButtonPressed(int buttonPressed) {
		float randomProjectileStartPosition = Random.Range(-1.25f,1.25f);
		Vector3 tempPos = playerWindow.transform.position;
		tempPos.x += randomProjectileStartPosition;
		if (buttonPressed == 1) {
			if (activeButton1 == "SWORD" && oreCount >= 1) {
				oreCount -= 1;
				Instantiate (playerAttackPrefabs[0], tempPos, playerWindow.transform.rotation);
			}
			if (activeButton1 == "RAGE" && manaCount >= 2) {
				manaCount -= 2;
				playerBuffs [0] = 10;
			}
		}
		if (buttonPressed == 2) {
			if (activeButton2 == "AXE" && oreCount >= 3) {
				oreCount -= 3;
				Instantiate (playerAttackPrefabs[1], tempPos, playerWindow.transform.rotation);
			}
			if (activeButton2 == "REGEN" && manaCount >= 4) {
				manaCount -= 3;
				playerBuffs [4] = 5;
			}
		}
		if (buttonPressed == 3) {
			if (activeButton3 == "LANCE" && oreCount >= 2) {
				oreCount -= 2;
				Instantiate (playerAttackPrefabs[2], tempPos, playerWindow.transform.rotation);
			}
			if (activeButton3 == "GUARD" && manaCount >= 2) {
				manaCount -= 2;
				playerBuffs [1] = 10;
			}
		}
		if (buttonPressed == 4) {
			if (activeButton4 == "HAMMER" && spiritCount >= 10) {
				spiritCount -= 10;
				Instantiate (playerAttackPrefabs[4], tempPos, playerWindow.transform.rotation);
			}
			if (activeButton4 == "COUNTER" && spiritCount >= 10) {
				spiritCount -= 10;
				playerBuffs [3] = 10;
			}
		}
	}
	public void UpdateButtons(string buttonType) {
		if (buttonType == "weapons") {
			button1.GetComponent<ActionButtons> ().UpdateButtonImage ("attack");
			button2.GetComponent<ActionButtons> ().UpdateButtonImage ("attack");
			button3.GetComponent<ActionButtons> ().UpdateButtonImage ("attack");
			//button4.GetComponent<ActionButtons> ().UpdateButtonImage ("attack");
			button1.transform.GetChild (0).GetChild (0).GetComponent<Text> ().text = "SWORD\n" + "ORE:1";
			button2.transform.GetChild (0).GetChild (0).GetComponent<Text> ().text = "AXE\n" + "ORE:3";
			button3.transform.GetChild (0).GetChild (0).GetComponent<Text> ().text = "LANCE\n" + "ORE:2";
			//button4.transform.GetChild (0).GetChild (0).GetComponent<Text> ().text = "HAMMER\n" + "SPIRIT:10";
			activeButton1 = "SWORD";
			activeButton2 = "AXE";
			activeButton3 = "LANCE";
			activeButton4 = "HAMMER";
		}
		if (buttonType == "abilities") {
			button1.GetComponent<ActionButtons> ().UpdateButtonImage ("ability");
			button2.GetComponent<ActionButtons> ().UpdateButtonImage ("ability");
			button3.GetComponent<ActionButtons> ().UpdateButtonImage ("ability");
			//button4.GetComponent<ActionButtons> ().UpdateButtonImage ("ability");
			button1.transform.GetChild (0).GetChild (0).GetComponent<Text> ().text = "RAGE\n" + "MANA:2";
			button2.transform.GetChild (0).GetChild (0).GetComponent<Text> ().text = "REGEN\n" + "MANA:4";
			button3.transform.GetChild (0).GetChild (0).GetComponent<Text> ().text = "GUARD\n" + "MANA:2";
			//button4.transform.GetChild (0).GetChild (0).GetComponent<Text> ().text = "COUNTER\n" + "SPIRIT:10";
			activeButton1 = "RAGE";
			activeButton2 = "REGEN";
			activeButton3 = "GUARD";
			activeButton4 = "COUNTER";
		}
	}
	public void ProjectileHit(string attack,bool playerProjectile, Vector3 damageTextPosition) {
		int attackModifier = 0;
		int damage = 0;
		if (attack == "sword") {
			attackModifier = 3;
		}
		if (attack == "axe") {
			attackModifier = 4;
			if(Random.Range(1,5) > 4) enemyStatus [1] = 10;
		}
		if (attack == "lance") {
			attackModifier = 2;
			if(Random.Range(1,5) > 3) enemyStatus [0] = 10;
		}
		if (attack == "slimeball") {
			attackModifier = 1;
		}
		if (playerProjectile) {
			damage = 10 * attackModifier * (2*playerPower - enemyDefense) / (20 + Random.Range(0,10));
			if(playerBuffs[0] > 0) damage *= 2;
			if(enemyStatus[1] > 0) damage *= 2;
			enemyCurrentHP -= damage;
		} else {
			damage = 10 * attackModifier * (2*enemyPower - playerDefense) / (20 + Random.Range(0,10));
			if (enemyStatus [0] > 0) damage /= 2;
			playerCurrentHP -= damage;
		}
		ShowDamage (damage, playerProjectile, damageTextPosition);
	}
	void ApplyStatus() {
		if (playerBuffs [4] > 0) playerCurrentHP += 1;
		if (enemyBuffs [4] > 0) enemyCurrentHP += 1;
	}


	void UpdateStatus() {
		for (int i = 0; i < playerStatusContainer.Length; i++) {
			playerStatusSpriteRenderer[i].sprite = null;
		}
		for (int i = 0; i < enemyStatusContainer.Length; i++) {
			enemyStatusSpriteRenderer[i].sprite = null;
		}
		for (int i = 0; i < playerBuffContainer.Length; i++) {
			playerBuffSpriteRenderer[i].sprite = null;
		}
		for (int i = 0; i < enemyBuffContainer.Length; i++) {
			enemyBuffSpriteRenderer[i].sprite = null;
		}
		int statusCount = 0;
		for (int i = 0; i < playerBuffs.Length; i++) {
			if (playerBuffs [i] > 0 && statusCount <= 2 && i != 3) { // i != 3 excludes life steal buff.
				playerBuffSpriteRenderer[statusCount].sprite = buffSpriteContainer [i];
				statusCount += 1;
			}
		}
		statusCount = 0;
		for (int i = 0; i < playerStatus.Length; i++) {
			if (playerStatus [i] > 0 && statusCount<= 2) {
				playerStatusSpriteRenderer[statusCount].sprite = statusSpriteContainer [i];
				statusCount += 1;
			}
		}
		statusCount = 0;
		for (int i = 0; i < enemyBuffs.Length; i++) {
			if (enemyBuffs [i] > 0 && statusCount<= 2 && i != 3) { // i != 3 excludes life steal buff.
				enemyBuffSpriteRenderer[statusCount].sprite = buffSpriteContainer [i];
				statusCount += 1;
			}
		}
		statusCount = 0;
		for (int i = 0; i < enemyStatus.Length; i++) {
			if (enemyStatus [i] > 0 && statusCount<= 2) {
				enemyStatusSpriteRenderer[statusCount].sprite = statusSpriteContainer [i];
				statusCount += 1;
			}
		}
	}
	void ClearStatus() {
		for (int i = 0; i < enemyStatus.Length; i++) {
			if(enemyStatus [i] > 0) enemyStatus[i] = 0;
		}
		for (int i = 0; i < enemyBuffs.Length; i++) {
			if(enemyBuffs [i] > 0) enemyBuffs[i] = 0;
		}
		for (int i = 0; i < playerStatus.Length; i++) {
			if(playerStatus [i] > 0) playerStatus[i] = 0;
		}
		for (int i = 0; i < playerBuffs.Length; i++) {
			if(playerBuffs [i] > 0) playerBuffs[i] = 0;
		}
		UpdateStatus ();
	}
	public void ShowDamage(int damage, bool playerProjectile, Vector3 damageTextPosition) {
		if (playerProjectile) {
			damageTextPosition.y -= 1f;
		} else {
			damageTextPosition.y += 1f;
		}
		damageTextPosition.x -= 0.2f;
		if (playerProjectile) {
			GameObject spawnedTextInstantiate = Instantiate (enemyDamageText, damageTextPosition, Quaternion.identity);
			GameObject spawnedText = spawnedTextInstantiate.transform.GetChild (0).GetChild (0).gameObject;
			spawnedText.GetComponent<Text> ().text = damage.ToString ();
		} else {
			GameObject spawnedTextInstantiate = Instantiate (playerDamageText, damageTextPosition, Quaternion.identity);
			GameObject spawnedText = spawnedTextInstantiate.transform.GetChild (0).GetChild (0).gameObject;
			spawnedText.GetComponent<Text> ().text = damage.ToString ();
		}
	}
}