﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TabButtonScript : MonoBehaviour {
	public bool weaponTab;
	public GameObject battleManager;
	public GameObject bottomWindow;
	public Sprite weaponTabSelected;
	public Sprite abilitiesTabSelected;

	void OnMouseDown() {
		if (weaponTab) {
			battleManager.GetComponent<NewBattleManager> ().UpdateButtons ("weapons");
			bottomWindow.GetComponent<SpriteRenderer> ().sprite = weaponTabSelected;
		} else {
			battleManager.GetComponent<NewBattleManager> ().UpdateButtons ("abilities");
			bottomWindow.GetComponent<SpriteRenderer> ().sprite = abilitiesTabSelected;
		}
	}
}
