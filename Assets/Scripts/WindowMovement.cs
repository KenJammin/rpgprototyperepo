﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WindowMovement : MonoBehaviour {
	public Vector2 hiddenPosition;
	private Vector2 visablePosition;
	private Vector2 tempPos;
	public int moveSpeed;
	public bool sideWindow;
	public bool leftSideWindow;
	public bool bottomWindow;
	private bool moveInWindow;
	private bool windowInMotion;

	void Start () {
		visablePosition = transform.position;
		hiddenPosition = transform.position;
		if (sideWindow && leftSideWindow && !bottomWindow) hiddenPosition.x -= 10;
		if (sideWindow && !leftSideWindow && !bottomWindow) hiddenPosition.x += 10;
		if (!sideWindow && bottomWindow && !leftSideWindow) hiddenPosition.y -= 10;
		if (!sideWindow && !bottomWindow && !leftSideWindow) hiddenPosition.y += 10;
		tempPos = hiddenPosition;
		transform.position = tempPos;
	}

	void Update () {
		if (windowInMotion) {
			if (!sideWindow) {
				if (!moveInWindow) {
					if (bottomWindow) {
						if (transform.position.y >= hiddenPosition.y) {
							tempPos.y -= moveSpeed * Time.deltaTime;
						} else {
							tempPos = hiddenPosition;
							windowInMotion = false;
						}
					} else {
						if (transform.position.y <= hiddenPosition.y) {
							tempPos.y += moveSpeed * Time.deltaTime;
						} else {
							tempPos = hiddenPosition;
							windowInMotion = false;
						}
					}
				} else {
					if (bottomWindow) {
						if (transform.position.y <= visablePosition.y) {
							tempPos.y += moveSpeed * Time.deltaTime;
						} else {
							tempPos = visablePosition;
							windowInMotion = false;
						}
					} else {
						if (transform.position.y >= visablePosition.y) {
							tempPos.y -= moveSpeed * Time.deltaTime;
						} else {
							tempPos = visablePosition;
							windowInMotion = false;
						}
					}
				}
			} else {
				if (moveInWindow) {
					if (leftSideWindow) {
						if (transform.position.x <= visablePosition.x) {
							tempPos.x += moveSpeed * Time.deltaTime;
						} else {
							tempPos = visablePosition;
							windowInMotion = false;
						}
					} else {			
						if (transform.position.x >= visablePosition.x) {
							tempPos.x -= moveSpeed * Time.deltaTime;
						} else {
							tempPos = visablePosition;
							windowInMotion = false;
						}
					}
				} else {
					if (leftSideWindow) {
						if (transform.position.x >= hiddenPosition.x) {
							tempPos.x -= moveSpeed * Time.deltaTime;
						} else {
							tempPos = hiddenPosition;
							windowInMotion = false;
						}
					} else {
						if (transform.position.x <= hiddenPosition.x) {
							tempPos.x += moveSpeed * Time.deltaTime;
						} else {
							tempPos = hiddenPosition;
							windowInMotion = false;
						}
					}
				}
			}
			transform.position = tempPos;
		}
	}
	public void MoveInWindow() {
		moveInWindow = true;
		windowInMotion = true;
	}
	public void MoveOutWindow(){
		moveInWindow = false;
		windowInMotion = true;
	}
}
