﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class RetryButton : MonoBehaviour {
	public Sprite blueButtonSprite;
	public Sprite blueButtonPushedSprite;

	void OnMouseDown() {
		SceneManager.LoadScene ("RpgPrototype");
		GetComponent<SpriteRenderer> ().sprite = blueButtonPushedSprite;
	}
	void OnMouseUp() {
		GetComponent<SpriteRenderer> ().sprite = blueButtonSprite;
	}
}
