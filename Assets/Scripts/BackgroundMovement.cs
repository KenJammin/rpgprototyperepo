﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackgroundMovement : MonoBehaviour {
	public float moveSpeed;
	public float resetTime;
	private Vector2 tempPos;
	private Vector2 startPos;
	private float resetTimer;

	// Use this for initialization
	void Start () {
		startPos = transform.position;
		tempPos = transform.position;
		resetTimer = resetTime;
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		resetTimer -= 1 * Time.deltaTime;
		tempPos.y += moveSpeed * Time.deltaTime;
		tempPos.x += moveSpeed * Time.deltaTime;
		transform.position = tempPos;
		if (resetTimer <= 0) {
			resetTimer = resetTime;
			tempPos = startPos;
		}
	}
}
