﻿using UnityEngine;
using System.Collections;

public class FontPoint : MonoBehaviour {
	void Start () {
		TextMesh text = GetComponent<TextMesh>();
		text.font.material.mainTexture.filterMode = FilterMode.Point;
	}
}